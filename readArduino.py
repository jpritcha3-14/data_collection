import serial
import time
import os
import sys 

class Sensor:

    def __init__(self, dev: str):
        self.device = dev
    
    def read(self, samples: int):
        port = serial.Serial(self.device)
        time.sleep(1)
        with port as s:
            loop_cout = 0
            _ = port.read_all()
            ans = []
            while loop_cout < samples:
                if s.read() == b'\r':
                    val = []
                    while True:
                        cur = s.read()
                        if cur == b'\r':
                            break
                        else:
                            val.append(cur)
                    int_val = int(''.join(map(lambda x: x.decode('utf-8'), val[1:])))
                    print(int_val)
                    ans.append(int_val)
                    loop_cout += 1
                    time.sleep(0.1)
                    _ = port.read_all()
            avg = sum(ans) / samples
            print(avg)
            return avg
                    
if __name__ == '__main__':
    device_name = input('Enter device name (blank=/dev/ttyACM0): ') or '/dev/ttyACM0'
    if not os.path.exists(device_name):
        raise FileNotFoundError('{} not found'.format(device_name))
    sensor = Sensor(device_name or '/dev/ttyACM0')
    file_name = input('Enter output filename: ')
    num_samples = int(input('Enter desired samples: '))
    delay = int(input('Enter delay before each set of samples: '))
    inc_val = 1
    while True:
        message = 'Enter the current mass, 0 to exit, blank to increment ({}): '.format(inc_val)
        cur_mass = input(message)
        if cur_mass == '0':
            break
        elif cur_mass == '':
            cur_mass = str(inc_val)
        else:
            inc_val = int(cur_mass) 
        inc_val += 1
        cur_delay = delay
        print('COUNTDOWN: ')
        while cur_delay > 0:
            print(cur_delay, ' ', end='' if cur_delay > 1 else '\n')
            sys.stdout.flush()
            time.sleep(1)
            cur_delay -= 1
        avg = sensor.read(num_samples);
        with open(os.path.join(os.getcwd(), file_name), "a") as fd:
            fd.write(','.join([cur_mass, str(avg)]) + '\n')
