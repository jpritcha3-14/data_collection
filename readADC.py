import board
import busio
import time

import adafruit_ads1x15.ads1015 as ADS

from  adafruit_ads1x15.analog_in import AnalogIn 

class ADC:
    def __init__(self):
        i2c = busio.I2C(board.SCL, board.SDA)
        ads = ADS.ADS1015(i2c)
        self.channel = [AnalogIn(ads, val) for val in [ADS.P0, ADS.P1, ADS.P2, ADS.P3]]

if __name__ == '__main__':
    test = ADC()
    while True:
        print(test.channel[0].value)
        time.sleep(0.25)
