### Introduction
This repo contains scripts for serial data collection of analog signals

## Dependencies
### Arduino
pyserial
```
pip install pyserial
```

### RPi
adafruit-circuitpython-lis3dh
adafruit-circuitpython-ads1x15
```
pip install adafruit-circuitpython-lis3dh
pip install adafruit-circuitpython-ads1x15
```
## Setup (Arduino)
This setup allows the Arduino to sample an analog value, which can then be read and stored by a host computer.

1. Flash the arduino with the sample code [listed here](https://www.arduino.cc/reference/en/language/functions/analog-io/analogread/).  This will set the Arduino to continuously report the analog value on pin A3 at a rate of 9600 baud.
2. Setup the Arduino hardware with a voltage divider according to the diagram below:

![diagram](/diagram.png)

3. Connect the Arduino to the host computer, find the device name in /dev, and run the readArduino.py script (It will prompt for the device name, output file, and number of samples).

## Setup (RPi)
This setup allows an RPi to sample values through GPIO using the ADS1015 ADC breakout
board. 

1. Follow [this wiring diagram](https://learn.adafruit.com/assets/58680) to
connect the the ADS1015 module to the RPi.  Take note that the module VDD pin is
connected to the 3.3V supply pin on the RPi, since this matches the RPi GPIO voltage.

2. Shunt the address pin to one of GND, VDD, SCL, or SDA to set the I2C
address of the ADS1015 chip.  Refer to [this
table](https://learn.adafruit.com/adafruit-4-channel-adc-breakouts/assembly-and-wiring#i2c-addressing-2974117-12)
for address values.

3. Connect the desired single ended connection to pin A0 ib tge ADS1015 (e.g. the Vout signal from the voltage divider in the Arduino setup section)  **!!!IMPORTANT!!!** Make sure that the maximum value of the analog signal being read matches the voltage of VDD supplied to the ADS1015 (3.3V in the case of RPi)
